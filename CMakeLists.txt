cmake_minimum_required(VERSION 2.6)


# Enable debug symbols by default
# must be done before project() statement
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build (Debug or Release)" FORCE)
endif()
# (you can also set it on the command line: -D CMAKE_BUILD_TYPE=Release)

project(gcs)

include_directories("${PROJECT_BINARY_DIR}")

file (GLOB_RECURSE SOURCES "./src/*.cpp")
set(EXECUTABLE_NAME "ripples")
add_executable(${EXECUTABLE_NAME} ${SOURCES})

find_package( Boost )
find_package(SFML 2.5 REQUIRED system window graphics audio)

if(SFML_FOUND)
  include_directories(${SFML_INCLUDE_DIR})
  target_link_libraries(${EXECUTABLE_NAME} sfml-graphics sfml-audio sfml-window sfml-system)
endif()

install(TARGETS ${EXECUTABLE_NAME} DESTINATION bin)
