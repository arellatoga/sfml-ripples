#include <SFML/Graphics.hpp>
#include "ecs/system/System.hpp"
#include "ecs/system/DrawableSystem.hpp"
#include "ecs/component/Ripple.hpp"
#include <cstdio>
#include <cstdlib>
#include <ctime>

int main() {
    srand(time(NULL));
    sf::RenderWindow window(sf::VideoMode(480, 320, 9), "Ripples");
    window.setFramerateLimit(60);
    sf::Time sftime;
    sf::Clock clock;
    DrawableSystem drawableSystem(window, sftime);

    sf::Time maxDelta = sf::microseconds(16667);
    sf::Time currentDelta = sf::microseconds(0);

    sf::View view;
    view = sf::View(sf::FloatRect(0.f, 0.f, 480.f, 320.f));
    window.setView(view);

    sf::Event event;
    while (window.isOpen()) {
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Resized) {
                sf::Vector2u size = window.getSize();
                printf("%f, %f\n", (float)size.x/(float)size.y, 3.f/2.f);
                if ((float)size.x/(float)size.y != 3.f/2.f) {
                    size.y = 2.f/3.f * size.x;
                }
                window.setSize(size);
                window.setView(view);
                printf("%d, %d\n", size.x, size.y);
            }
        }
        window.setActive();

        currentDelta += clock.getElapsedTime();

        if (currentDelta < maxDelta) {
            continue;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            sf::Vector2i pos = sf::Mouse::getPosition(window);

            float xRatio = (float)(window.getSize().x) / 240.f;
            float yRatio = (float)(window.getSize().y) / 160.f;

            Ripple* ripple = new Ripple(0, 0, 0, pos.x * xRatio -50, pos.y * yRatio - 50);

            drawableSystem.Add(ripple);
        }

        window.clear();

        drawableSystem.Run(window);

        window.display();

        currentDelta = sf::microseconds(0);
    }

    return EXIT_SUCCESS;
}
