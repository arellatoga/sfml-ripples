#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <vector>

#include "../ecs/system/System.hpp"

template <class T>
class World {
    private:
    sf::Clock& _clock;
    const sf::RenderWindow& _renderWindow;
    std::vector<System*> _systems;

    public:
    World(sf::Clock& clock, sf::RenderWindow& renderWindow):
        _clock { clock },
        _renderWindow { renderWindow } {
    }

    const sf::Clock& getClock() {
        return _clock;
    }
};
