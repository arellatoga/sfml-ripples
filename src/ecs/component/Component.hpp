#pragma once

class Component {
    public:
    virtual void Run() = 0;
};
