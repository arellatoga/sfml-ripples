#pragma once

#include <SFML/Graphics/Drawable.hpp>
#include "Component.hpp"

class DrawableComponent: public Component {
    public:

    DrawableComponent() {
    }

    DrawableComponent(sf::Drawable* drawable) {
        _drawable = drawable;
    }
    void Run() {
    }

    sf::Drawable* getDrawable() {
        return _drawable;
    }

    protected:
    sf::Drawable* _drawable;
};
