#include "Ripple.hpp"

void Ripple::Run() {
    if (_initialRadius < _radius) {
        _initialRadius += 0.25f;

        sf::CircleShape* circle = dynamic_cast<sf::CircleShape*>(_drawable);
        circle->setRadius(_initialRadius);
        circle->setOrigin(_initialRadius, _initialRadius);
    }
}
