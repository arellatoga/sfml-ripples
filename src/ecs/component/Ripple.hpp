#pragma once

#include <SFML/Graphics.hpp>
#include "DrawableComponent.hpp"

#include "../../util/Palette.hpp"

class Ripple : public DrawableComponent {
    private:
        // wave amplitude
        float _amplitude;
        // wave decay
        float _wavelength;
        // distance in wavelengths
        float _distance;

        float _initialRadius;
        float _radius;

    public:
        Ripple(float amp, float lambda, float dist, float x, float y):
                _amplitude {amp},
                _wavelength {lambda},
                _distance {dist},
                _initialRadius {0},
                _radius {100} {

            Palette palette(3);
            sf::CircleShape* circle = new sf::CircleShape(0);
            circle->setFillColor(palette.getRandomColor());
            circle->setOutlineColor(palette.getRandomColor());
            circle->setOutlineThickness(2.0f);
            circle->setPosition(x + _radius/2, y + _radius/2);
            _drawable = circle;
        }

        void Run();
};
