#pragma once

#include<vector>
#include <SFML/Graphics.hpp>

#include "../component/Component.hpp"

class System {
    protected:
    std::vector<Component*> _components;

    public:
    virtual void Run() = 0;
    //virtual void Add() = 0;
};
