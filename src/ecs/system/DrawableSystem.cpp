#include <vector>
#include <cstdio>
#include <SFML/Graphics.hpp>

#include "DrawableSystem.hpp"
#include"../component/DrawableComponent.hpp"


void DrawableSystem::Run(sf::RenderWindow& renderWindow) {
    for (Component* _component : _components) {
        DrawableComponent* _drawableComp = dynamic_cast<DrawableComponent*>(_component);
        _drawableComp->Run();

        renderWindow.draw(*_drawableComp->getDrawable());
    }
}

DrawableComponent* DrawableSystem::Add(DrawableComponent* drawableComponent) {
    _components.push_back(drawableComponent);

    return drawableComponent;
}

DrawableSystem::~DrawableSystem() {
    for (Component* _component : _components) {
        DrawableComponent* _drawableComp = dynamic_cast<DrawableComponent*>(_component);
        delete _drawableComp->getDrawable();
    }
}
