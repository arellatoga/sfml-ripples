#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Time.hpp>
#include <vector>
#include "System.hpp"
#include "../component/DrawableComponent.hpp"

class DrawableSystem : public System {
    private:
    sf::RenderWindow& _renderWindow;
    public:
    sf::Time& _time;
    DrawableSystem();
    DrawableSystem(sf::RenderWindow& renderWindow, sf::Time& time):
        _renderWindow{renderWindow},
        _time{time} { };
    ~DrawableSystem();
    void Run() {
        Run(_renderWindow);
    }
    void Run(sf::RenderWindow&);
    DrawableComponent* Add(DrawableComponent*);
};
