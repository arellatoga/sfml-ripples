#pragma once

#include <SFML/Graphics/Color.hpp>

class Palette {
    public:
    const int _bits;
    Palette(int bits): _bits { bits } {
    }
    sf::Color getColor(int, int, int, int);
    sf::Color getRandomColor();

    private:
    int nBitMaxVal(int);
};
