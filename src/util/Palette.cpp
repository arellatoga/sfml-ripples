#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <SFML/Graphics/Color.hpp>
#include "Palette.hpp"

sf::Color Palette::getColor(int red, int green, int blue, int alpha = 255) {
    float ratio = 255/(float)nBitMaxVal(_bits);
    int redAdj = round(red * ratio);
    int blueAdj = round(blue * ratio);
    int greenAdj = round(green * ratio);
    return sf::Color(redAdj, greenAdj, blueAdj, round(alpha * ratio));
}

sf::Color Palette::getRandomColor() {
    int max = nBitMaxVal(_bits);
    int red = rand() % (max+1);
    int green = rand() % (max+1);
    int blue = rand() % (max+1);
    return getColor(red,
                    green,
                    blue,
                    255);
}

int Palette::nBitMaxVal(int bits) {
    int i = 1;
    while (--bits > 0) {
        (i <<= 1)++;
    }

    return i;
}
